# kubernetes

* https://kubernetes.io/


## 簡介

* https://zh.wikipedia.org/wiki/Kubernetes
* [Kubernetes 30天學習筆記系列 第 1 篇](https://ithelp.ithome.com.tw/articles/10192401)

Kubernetes 是一個協助我們自動化部署、擴張以及管理容器應用程式(containerized applications)的系統。相較於需要手動部署每個容器化應用程式(containers)到每台機器上，Kubernetes 可以幫我們做到以下幾件事情：

同時部署多個 containers 到一台機器上，甚至多台機器。
管理各個 container 的狀態。如果提供某個服務的 container 不小心 crash 了，Kubernetes 會偵測到並重啟這個 container，確保持續提供服務
將一台機器上所有的 containers 轉移到另外一台機器上。
提供機器高度擴張性。Kubernetes cluster 可以從一台機器，延展到多台機器共同運行。