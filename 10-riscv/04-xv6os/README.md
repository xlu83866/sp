# xv6

* [xv6 的建置與執行](00-build)
* [xv6 的函數介面 -- Interface](01-util)
    * [Lab1 - util](01-util)
* [xv6 的系統呼叫 -- System Call](02-syscall)
    * [Lab2 - syscall](02-syscall)
* [xv6 的分頁機制 -- Page Table](03-pgtbl)
    * [Lab3 - pgtbl](03-pgtbl)
* [xv6 的中斷與陷阱 -- traps](04-traps)
    * [Lab4 - traps](04-traps)
* [xv6 的驅動程式 -- driver](04-traps)
    * [Lab11 - net](11-net)
* [xv6 的記憶體管理 -- Memory](05-memory)
    * [Lab5 - Lazy Allocation](05-lazy)
    * [Lab6 - Copy on Write](06-cow)
* [xv6 的鎖 -- lock](08-lock)
* [xv6 的檔案系統](02-syscall)
* [xv6 的網路通訊](02-syscall)


## 程式來源與授權聲明

本資料夾主要程式來源有下列幾個，使用時請遵守其 [《授權聲明》](https://github.com/mit-pdos/xv6-riscv/blob/riscv/LICENSE) ：

* [6.S081: Operating System Engineering](https://pdos.csail.mit.edu/6.828/2020/)
    * https://github.com/mit-pdos/xv6-riscv
* [MIT 6.828 目录页](https://blog.mky.moe/mit6828/menu/)
    * https://github.com/monkey2000/xv6-riscv-fall19
* [build a xv6 os](https://xiayingp.gitbook.io/build_a_os/)
    * https://github.com/gigimushroom/mushroom_os
